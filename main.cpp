// Copyright 2019 , Dr. Srijith Rajamohan, All Rights Reserved

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <ctime>
#include <math.h>
#include <mpi.h>
#include "routines.h"

#define row_index 3
#define col_index 3

using namespace std;

void QR(Matrix &A,Matrix &Rmatrix, int row, int col);
void fileread(Comp_Matrix &A,int my_rank,int np,int &process_start,char filename[]);
void fileread2(Comp_Matrix &A,int my_rank,int np);
void read_rhs(Vec &b , char filename[]);

void shift( Matrix &H, double *eig, int k, int size ,double &shift_val)
{

    int i;

    if ( k >= 0)
    {

        shift_val = eig[k];

        for ( i = 0 ; i <= size ; i++)
            H.x[i][i] -= shift_val;

    }

}


void unshift( Matrix &H, double *eig, int k, int size , double &shift_val)
{

    int i;

    if ( k >= 0)
    {

        shift_val = eig[k];

        for ( i = 0 ; i <= size ; i++)
            H.x[i][i] += shift_val;

    }

}


void shift( Matrix &H, int k, double &shift_val)
{

    int i;

    if ( k > 0)
    {

        for ( i = 0 ; i <= k ; i++)
            H.x[i][i] -= shift_val;

    }

}


void fill_eig(double *&eig , Matrix &H , int k ,double &shift_val)
{

    int i;

    i = k;

    {
        eig[i] = H.x[i][i] ;
    }

}



void complex_fill_eig(double *&eig , double mag , int k )
{

    eig[k] = mag;
    eig[k - 1] = mag;

}

void complex_solve(Matrix &H, int k , double &real1 , double &real2, double &imag1 , double &imag2)
{

    double a,b,c,d;

    a = H.x[k][k];
    b = H.x[k][k+1];
    c = H.x[k+1][k];
    d = H.x[k+1][k+1];

    real1 = 0.5*(a + d);
    real2 = real1;

    imag1 = 0.5*sqrt(-1.0*(4*b*c + (a - d)*(a - d)));
    imag2 = -0.5*sqrt(-1.0*(4*b*c + (a - d)*(a - d)));

    if ( imag1 != imag1 || imag2 != imag2 )
    {

        cout<<"Nan";

    }

}

void complex_check()
{

// Not implemented

}



void f_print(Comp_Matrix &A, FILE *fp,int process_start)
{

    int i,j;
    int count = 0;


    for ( i = 0 ; i < A.nrow ; i++)
    {
        for ( j =  A.ia[i] ; j < A.ia[i + 1] ; j++)
        {

            
            fprintf(fp," %lf (%d,%d) \t ",A.x[count],i,A.ja[count]);  // in sub process this becomes A.x[ j - process_start],A.ja[j - process_start]
            count++;

        }

        fprintf(fp,"\n\n");
    }

}

void f_print(Comp_Matrix &A, int my_rank)
{

    int i,j;
    FILE *fp;
    char name[132];
    name[0] = '\0';
    strcat(name,"A");
    sprintf(name,"%s%d",name,my_rank);
    fp = fopen(name,"w");
    if (fp == NULL ) cout<<" File open failed \n";

    //  cout<<my_rank<<" going from row 0 to "<<A.nrow<<" elem "<<A.ia[0 + 1]<<" - "<<A.ia[0]<<endl;fflush(stdout);

    for ( i = 0 ; i < A.nrow ; i++)
    {
        for ( j = 0 ; j < A.ia[i + 1] - A.ia[i] ; j++)
        {

            fprintf(fp," %lf (%d) \t ",A.x[j],A.ja[j]);

        }

        fprintf(fp,"\n");
    }

    fclose(fp);

}

 //////////////// BACKSUBSTITUTION /////////////////////////////////


void backsubstitution(Matrix &Rmatrix , Vec &rhs , int num_rows, int num_cols , Matrix &Q, double *X, int my_rank, int size)
{

  int i , j;
  double sum;
  int r = num_rows;
  int c = num_cols;

  Vec soln_part(size);   // small chunk size for each process

  #if 1

  Vec soln(r);

     for (i = num_cols - 1; i >= 0; i--)
    {
        soln.x[i] = rhs.x[i];
        for (j = i + 1; j < num_cols ; j++)
        {
            soln.x[i] -= Rmatrix.x[i][j] * soln.x[j];
        }
        soln.x[i] /= Rmatrix.x[i][i];
    }

    cout<<endl<<" ------------------ Solution -----------------"<<endl;

    //soln.print(num_cols);

   for ( i = 0 ; i < Q.row_ind ; i++)
   {

    sum = 0.0;

      for ( j = 0 ; j < Q.col_ind ; j++)
      {

       sum += Q.x[i][j] * soln.x[j];

      }

      soln_part.x[i] = sum;

   }


   for ( i = 0 ; i < size ; i++)
   {

        soln_part.x[i] += X[my_rank * size + i];
        cout<<my_rank<<" ----> "<<soln_part.x[i]<<"\t";

   }
   cout<<endl;

    #endif
}



////////////////// NORM CALCULATION ///////////////////////////////


void calc_norm(int np,int my_rank,int root, Vec &part, int size, Vec &b , Comp_Matrix &A, Vec &g, double *X)
{

    int i,j;
    double *send_list,*recv_list;
    int cnt;
    double sum = 0.0;
    double totalsum = 0.0;

// size = total size;

    cnt = size / np ;
    part.col_ind = cnt;

    send_list = (double*)calloc(cnt,sizeof(double));

    if ( send_list == NULL )
    {
        cout<<"send list alloc failed \n";
        fflush(stdout);
        exit(0);
    }

    mat_mul(A , X , send_list , cnt);
    mat_sub_part(b , send_list , send_list , cnt , my_rank );

    for ( i = 0 ; i < cnt ; i++)
    {

        sum += send_list[i] * send_list[i] ;

    }

    MPI_Allreduce(&sum,&totalsum,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
    totalsum = sqrt(totalsum);

    for ( j = 0 ; j < cnt ; j++)
    {

        part.x[j] =  send_list[j] / totalsum;

    }

    g.x[0] = totalsum;


    free(send_list);
// free(recv_list);

    cout<<" ----------------- At the end of norm ---------------- "<<endl;

}



///////////////////////  MATRIX MULTIPLICATION ///////////////////


void parallel_mat_mul(Comp_Matrix &A,Vec &qk,Vec &uk,int np,int *transfer_map,int *recv_map, Col &X, int root, int my_rank)
{

    int i,j,count;
    MPI_Request Req;
    MPI_Status Stat;
    double *temp,*temp2,*uk_received;
    double sum;
    temp = (double*)calloc(qk.col_ind,sizeof(double));
    temp2 = (double*)calloc(qk.col_ind,sizeof(double));
    uk_received = (double*)calloc(qk.col_ind*np,sizeof(double));

    cout<<endl;

    //////////// SEND VECTOR DATA CORRESPONDING TO COLUMN IN COMPRESSED MATRIX COLUMNS /////

    for ( i = 0 ; i < qk.col_ind ; i++) temp[i] = qk.x[i];

    for ( i = 0 ; i < np ; i++)
    {

        if (transfer_map[i] < 0 ) continue ;

        MPI_Isend(temp, qk.col_ind, MPI_DOUBLE, i, 0, MPI_COMM_WORLD, &Req);    // send to process i

    }


    for ( i = 0 ; i < np ; i++)
    {

        if (recv_map[i] < 0 ) continue;

        MPI_Irecv(temp2, qk.col_ind, MPI_DOUBLE, recv_map[i], 0, MPI_COMM_WORLD, &Req );

        MPI_Wait(&Req,&Stat);

        for ( j = 0 ; j < qk.col_ind ; j++)
        {

            X.x[recv_map[i] * qk.col_ind + j ] = temp2[j];
            X.row_ptr[recv_map[i] * qk.col_ind + j ] = recv_map[i] * qk.col_ind + j ;

        }

    }


    count = 0;

    for ( i = 0 ; i < A.nrow ; i++)
    {

        sum = 0.0;
        for ( j =  A.ia[i] ; j < A.ia[i + 1] ; j++)
        {

            sum += A.x[count] * X.x[A.ja[count]] ;
            count++ ;
        }

        temp[i] = sum;
        uk.x[my_rank * A.nrow + i ] = sum;
    }


    MPI_Gather(temp, qk.col_ind, MPI_DOUBLE, uk_received, qk.col_ind , MPI_DOUBLE, root , MPI_COMM_WORLD );

    if (my_rank == root)
    {

        for (i = 0; i < uk.col_ind ; i++)  uk.x[i] = uk_received[i];

    }

    free(temp);

}



///////////////  INNER PRODUCT //////////////////


void parallel_inner_product(Vec &qj, Vec &uk, double &prod, int my_rank, int root)
{

    int i,j;
    double sum;
    int *map;

    sum = 0.0;

    for (i = 0 ; i < qj.col_ind ; i++)
    {

        sum += qj.x[i] * uk.x[my_rank * qj.col_ind + i] ;

    }

    MPI_Allreduce(&sum,&prod,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);

}



//////////////////  SCALAR MULTIPLICATION ////////////////////


void parallel_scalar_mult(Vec &qj, double *prod, double scalar, int root, int np, int my_rank, Vec &uk)
{

    int i,j;
    double *temp;
    temp = (double*)calloc(qj.col_ind,sizeof(double));

    for ( i = 0 ; i < qj.col_ind ; i++)
        uk.x[my_rank * qj.col_ind + i] -= scalar*qj.x[i];

    free(temp);

}



///////////////////// NORM CALCULATION ALONE ///////


void norm(Vec &uk, int np, int my_rank, int size, double &norm_val)
{

    int i,j;
    double result;

    result = 0.0;

    for ( i = 0 ; i < size ; i++)
    {

        result += uk.x[my_rank * size + i] * uk.x[my_rank * size + i];

    }

    MPI_Allreduce(&result,&norm_val,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
    norm_val = sqrt(norm_val);

}



///////////////////////////////  Q UPDATE /////////////////////////////////////

void update_Q(Matrix &Q, int k , double *prod, int root, int my_rank, int np)
{

    int i,j;

    double *temp;
    temp = (double*)calloc(Q.row_ind,sizeof(double));

    MPI_Scatter(prod, Q.row_ind, MPI_DOUBLE, temp, Q.row_ind, MPI_DOUBLE, root, MPI_COMM_WORLD);

    for ( i = 0 ; i < Q.row_ind ; i++)
        Q.x[i][k] = temp[i];

    free(temp);
}


void update_Q(Matrix &Q, int k, int my_rank, Vec &uk, double scalar)
{

    int i,j;

    for ( i = 0 ; i < Q.row_ind ; i++)
    {

        Q.x[i][k] = uk.x[my_rank * Q.row_ind + i] / scalar ;

    }

}





void setup_transfer_map(Comp_Matrix &A,int my_rank,int np,int *map,int *recv_map)
{

    int i,proc;

    int size_per_process = A.ncol / np;      // A.ncol is size of X vector

    for ( i = 0 ; i < A.num_elem ; i++)
    {

        proc = A.ja[i] / size_per_process ;
        map[proc] = my_rank + 1;

    }

    MPI_Alltoall(map, 1, MPI_INT, recv_map, 1, MPI_INT, MPI_COMM_WORLD);

    for ( i = 0 ; i < np ; i++)
    {

        recv_map[i] = recv_map[i] - 1;   // decrement index by 1
        map[i] = map[i] - 1;   // decrement index by 1

    }

}



int main(int argc, char* argv[])
{

    int my_rank,np,N;

    int i,j,k,l;
    double temp;
    int root,size;

    Comp_Matrix A(1,1,1,1,1);
    int *transfer_map,*recv_transfer_map;
    double *prod;
    char *filename;    


    MPI_Status status;
    MPI_Status stat;
    MPI_Request req,req_r[8];
    N = 1;             /////////////////////////////// CHANGE THIS
    np = 3;
    root = 0;

    ///// A - Input Matrix
    ///// Q - Q matrix;
    ///// H - Hessenberg Matrix


    MPI_Init(&argc,&argv);
    MPI_Comm_rank(MPI_COMM_WORLD,&my_rank);
    MPI_Comm_size(MPI_COMM_WORLD,&np);

    double conv;
    int iter;

    FILE *fp,*fp1,*fp2;
    char name[132];
    int process_start;

    filename = (char*)calloc(132 , sizeof(char));
    filename = argv[1];    
    name[0] = '\0';
    strcat(name,"Out");
    sprintf(name,"%s%d",name,my_rank);
    fp = fopen(name,"w");
    if (fp == NULL ) cout<<" File open failed \n";

    sprintf(name,"_eig%d",my_rank);
    fp1 = fopen(name,"w");
    if (fp1 == NULL ) cout<<" File open failed \n";

    transfer_map = (int*)calloc(np,sizeof(int));      // can optimize by making this a boolean variable
    recv_transfer_map = (int*)calloc(np,sizeof(int));      // can optimize by making this a boolean variable

    fileread(A,my_rank,np,process_start,filename);
    // f_print(A,fp,process_start);

#if 1
    prod = (double*)calloc(A.nrow*np,sizeof(double));
    size = A.nrow;
    iter = 42;

    cout<<endl<<" ------------------ Done allocating prod ------------------ ";
    fflush(stdout);

    Col X(A.nrow*np);     // initial vector of full size
    Matrix Q(A.nrow,iter + 1);
    cout<<endl<<" ------------------ Done allocating Q ------------------";
    fflush(stdout);

    Vec qk(A.nrow);
    Vec qj(A.nrow);

    cout<<endl<<" ------------------ Done allocating qj ------------------ ";
    fflush(stdout);

    Vec uk(A.nrow*np);
    Vec part(size);
    Vec b(A.nrow*np);   // read the wholw rhs in
    Vec c(A.nrow*np),s(A.nrow*np);
    Matrix H(1 + A.nrow*np, 1 + iter );
    Vec g(1 + A.nrow*np);


    double *eig, *X_init;
    eig = (double*)calloc(iter,sizeof(double));
    X_init = (double*)calloc(A.nrow * np ,sizeof(double));
    double shift_val;
    int complex_flag;
    int diverge, count;
    double real1,real2,imag1,imag2;
    double pre_conv,mag;
    int Hsize;
    double temp1;
    double del,gamma;
    double resid;

    if ((fp2=fopen("Convergence","w")) == 0)
    {
        printf("\nCouldn't open write file \n");
        exit(0);
    }

    cout<<endl<<"Number of rows -> "<<A.nrow<<" and number of processes -> "<<np<<endl;
    fflush(stdout);

    read_rhs(b,filename);

    cout<<" ------------------ Done with X ----------------- \n";
    fflush(stdout);

    calc_norm(np,my_rank,root,part,A.nrow*np,b,A,g,X_init);

    cout<<" ------------------ Done with norm ---------------- \n";
    fflush(stdout);

    setup_transfer_map(A,my_rank,np,transfer_map,recv_transfer_map);

    Q.column_replace(part,0);

    for ( k = 0 ; k < iter ; k++)
    {

        fflush(stdout);

        qk.x = Q.getcol(k);
        
        parallel_mat_mul(A,qk,uk,np,transfer_map,recv_transfer_map,X,root,my_rank);

        for ( j = 0 ; j <= k ; j++)
        {

            qj.x = Q.getcol(j);

            parallel_inner_product(qj,uk,H.x[j][k],my_rank,root);

            parallel_scalar_mult(qj, prod, H.x[j][k], root , np, my_rank, uk);

        }

        norm(uk, np, my_rank, size, H.x[k+1][k]);

        update_Q(Q,k+1,my_rank,uk,H.x[k+1][k]);
        

        /////////////////////////////   BEGIN GMRES CODE  /////////////////////////////


        /////////////////  GIVENS ROTATIONS  //////////


        for ( j = 0 ; j <= k - 1 ; j++)
        {

            del = H.x[j][k];

            H.x[j][k] = c.x[j] * del  +   s.x[j] * H.x[j+1][k] ;

            H.x[j+1][k] = c.x[j] * H.x[j+1][k]  -   s.x[j] * del ;

        }


        gamma = sqrt( H.x[k][k] * H.x[k][k]   +   H.x[k+1][k] * H.x[k+1][k]  );

        c.x[k] = H.x[k][k] / gamma ;

        s.x[k] = H.x[k+1][k] / gamma ;

        H.x[k][k] = gamma ;

        H.x[k+1][k] = 0.0;

        del = g.x[k] ;

        g.x[k] = c.x[k] * del  +  s.x[k] * g.x[k+1] ;

        g.x[k+1] = c.x[k] * g.x[k+1]  -  s.x[k] * del ;

        resid = g.x[k+1];
        if (my_rank == root) {cout<<" Residual in iteration "<<k<<" ========== "<<resid;  fprintf(fp2,"%.16lf \n",fabs(resid)); }

        if ( fabs(resid) < 10e-12 )
        {

            ///// BACKSUBSTITUTION /////

            backsubstitution(H , g , k+2 , k+1 , Q , X_init, my_rank, A.nrow);
            break;

        }


        /////////////////  END OF GIVENS ROTATIONS  ///////////


    }

      backsubstitution(H , g , k+2 , k+1 , Q , X_init, my_rank, A.nrow);

   if (my_rank == root)  cout<<"GMRES exited in iteration "<<k<<" with residual ============ "<<resid<<endl;

    free(eig);

    fclose(fp);
    fclose(fp1);
    fclose(fp2);


//   free(transfer_map);
//   free(recv_transfer_map);
//   free(prod);

#endif


    MPI_Finalize();

    return 0;

}

void read_rhs(Vec &b , char filename[])
{

 FILE *fp;
 char name[132]; 

    name[0] = '\0';
    strcat(filename,"_rhs.mtx"); 

    if ((fp=fopen(filename,"r")) == 0)
    {
        printf("\nCouldn't open rhs file %s \n",filename);
        exit(0);
    }

    const int bdim = 132;
    char buff[bdim];
    int row,col,row1,col1,nnz;
    double y;
    int i;

    fgets(buff,bdim,fp);
    sscanf(buff,"%d %d ",&row,&col);

    for ( i = 0 ; i < row ; i++)
    {

     fgets(buff,bdim,fp);
     sscanf(buff,"%lf",&b.x[i]);

    }



 fclose(fp);

}




void fileread(Comp_Matrix &A,int my_rank, int np,int &process_start, char filename[])
{

    int i,j;
    int row_major_flag = 0;
    char name[132];    

    FILE *fp;

    const int bdim = 132;
    char buff[bdim];
    int row,col,row1,col1,nnz;
    double y;
    int *offset,*row_size;
    //int process_start;
    int process_end,size,my_nnz,my_row,process_offset;
    size = 0;

    A.my_rank = my_rank;
    A.np = np;

    name[0] = '\0';
    strcat(name,filename);    
    strcat(name,".mtx");    

    if ((fp=fopen(name,"r")) == 0)
    {
        printf("\nCouldn't open matrix file %s \n",filename);
        exit(0);
    }

    fgets(buff,bdim,fp);
    sscanf(buff,"%d %d %d",&row,&col,&nnz);
    offset = (int*)calloc(row+1,sizeof(int));
    row_size = (int*)calloc(row+1,sizeof(int));

    my_row = row/np;
    process_start = my_rank * my_row;
    process_end   = process_start + my_row;

    //////// FIRST PASS THROUGH FILE /////

    for ( i = 0 ; i < nnz ; i++)
    {

        fgets(buff,bdim,fp);
        sscanf(buff,"%d %d %lf",&row1,&col1,&y);
        row_size[row1-1]++;

    }

    ///////// CALCULATE TOTAL SIZE OR NNZ OF LOCAL ROW /////

    for ( i = process_start ; i < process_end ; i++)
    {
        size += row_size[i];
    }

    cout<<my_rank<<"------------------- "<<size<<"--"<<process_start<<"--"<<process_end<<endl;

    A.Redimension(my_row,col,size,my_rank,np);
    A.num_elem = size;

    ///////// CALCULATE OFFSETS FOR THE WHOLE MATRIX ///////

    offset[0] = 0;

    for ( i = 1 ; i <= row ; i++)
    {
        offset[i] = offset[i-1] + row_size[i-1];
    }

    ///////////// CALCULATE IA FOR THE LOCAL MATRIX /////////

    for ( i = process_start ; i <= process_end ; i++)
    {
        A.ia[i - process_start ] = offset[i];
    }

    fflush(stdout);
    //////////// RESTART FROM BEGINNING OF FILE AND FILL IN THE LOCAL MATRIX /////

    fseek ( fp , 0 , SEEK_SET );
    fgets(buff,bdim,fp);

    int count = 0;

    if ( 0)
    {

        for ( i = 0 ; i < nnz ; i++)
        {

            fgets(buff,bdim,fp);

            if ( i < offset[process_start] ) continue ;
            if ( i >= offset[process_end] ) break ;

            sscanf(buff,"%d %d %lf",&row1,&col1,&y);
            row1--;
            col1--;
            A.x[count] = y;
            A.ja[count] = col1;
            count++;

        }

    }



    if (1)
    {

        process_offset = offset[process_start];
        cout<<"Process offset "<<process_offset<<endl;

        for ( i = 0 ; i < nnz ; i++)
        {

            fgets(buff,bdim,fp);
            sscanf(buff,"%d %d %lf",&row1,&col1,&y);

            row1--;
            col1--;

            if ( row1 < process_start ) continue;
            if ( row1 >= process_end  ) continue;

            A.x[ offset[row1] - process_offset ] = y;
            A.ja[ offset[row1] - process_offset ] = col1;

            offset[row1]++;

        }

    }

    free(offset);
    free(row_size);

    fclose(fp);

}


void QR(Matrix &A,Matrix &Rmatrix, int row, int col)
{

    int i,j,k;
    double row_norm,int2;
    Matrix B(row,col);
    Matrix rhs(row,1),soln(col,1),tempmat(row,1);
    Matrix C(row,col),rhs_copy(row,1);

    Vec v(row),u(row),w(row),t(row);
    double r;

    for (i = 0 ; i < col ; i++) // i < col
    {

        v.x = A.getcol(i);

        row_norm = norm(v);
        Rmatrix.x[i][i] = row_norm;

        scalar_mult(v,u,1/row_norm);

        A.column_replace(u,i);

        for ( k = i+1 ; k < col ; k++)
        {

            w.x = A.getcol(k);

            mat_mul(u,w,r);

            Rmatrix.x[i][k] = r;

            scalar_mult(u,t,r);

            mat_sub(w,t,w);

            A.column_replace(w,k);

        }


    }


}
