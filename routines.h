#include <math.h>
#define ind 4
#include <iostream>

using namespace std;

struct Node
{
    double element[ind];  // 2 element x solution vector

};


struct Block
{
    double element[ind][ind];  // 4 block sub matrix

};



class Col
{

 /////// col_ind is used to size of the vector , usually as number of rows inspite of the name

public:
  Col()
  { col_ind = 0;  x = NULL;
  row_ptr = NULL;
  num_elem = 0;

  }

  ~Col()
  {
   if (col_ind > 0 ) {free (x); free(row_ptr);}
  }

  Col(int k)
  {

   col_ind = k;
   x = (double*)calloc(k,sizeof(double));
   row_ptr = (int*)calloc(k,sizeof(int));

  }

   void Redimension(int row1)
  {

     int i;
     col_ind = row1;
      x = (double*)realloc(x,row1*sizeof(double));
      row_ptr = (int*)realloc(row_ptr,row1*sizeof(int));

  }

  void Add(double y,int row1)
  {

    col_ind++;
    x = (double*)realloc(x,(col_ind+1)*sizeof(double));
    row_ptr = (int*)realloc(row_ptr,(col_ind+1)*sizeof(int));

    x[col_ind] = y;
    row_ptr[col_ind] = row1;

  }


  void print()
  {
     int i;

     printf("\n");

     for(i = 0 ; i < col_ind ; i++)
     printf("%lf (%d) \t",x[i],row_ptr[i]);

      printf("\n");

  }


double *x;
int *row_ptr;
int col_ind;
int id;
int num_elem;
int max_index;

};


class Vec
{

 /////// col_ind is used to size of the vector , usually as number of rows inspite of the name

public:
  Vec()
  { col_ind = 0;  x = NULL;

  }

  ~Vec()
  {
   if (col_ind > 0 ) free (x);
  }

  Vec(int k)
  {

   col_ind = k;
   x = (double*)calloc(k,sizeof(double));


  }

  void Redimension(int k)
  {

   x = (double*)realloc(x,k*sizeof(double));
   col_ind = k;

  }



  void print()
  {
     int i;

     printf("\n");

     for(i = 0 ; i < col_ind ; i++)
     printf("%lf \t",x[i]);

      printf("\n");

  }


  void print(int row1)
  {
     int i;

     printf("\n");

     for(i = 0 ; i < row1 ; i++)
     printf("%lf \t",x[i]);

      printf("\n");

  }



double *x;
int col_ind;
int id;

};






void mat_add(Vec *A,Vec *B,Vec *C,int k)
{


 int i,j;

 for (i = 0 ; i < k ; i++)
    C->x[i] = A->x[i] + B->x[i];



}


void mat_sub(Vec &A,Vec &B,Vec &C)
{


 int i,j;

 for (i = 0 ; i < A.col_ind ; i++)
    C.x[i] = A.x[i] - B.x[i];



}


void mat_sub_part(Vec &A,double *B, double *C, int size, int my_rank)   // not generic
{


 int i,j;

 for (i = 0 ; i < size ; i++)
    C[i] = A.x[my_rank * size + i] - B[i];

}


class Matrix
{

  public:
  Matrix()
  {

      row_ind = 0; col_ind = 0;

  }

  ~Matrix()
  {
     int i;

     if( row_ind > 0 && col_ind > 0)
     {
     for(i = 0 ; i < row_ind ; i++)
      {
       free(x[i]);
      }

      free(x);
     }
  }

  Matrix(const Matrix& A)
  {
   int i,j;

   for(i = 0 ; i < row_ind ; i++)
    for(j = 0; j < col_ind ; j++)
     x[i][j] = A.x[i][j];

  }

  Matrix(int row1,int col1)
  {
      int i;
      col_ind = col1;
      row_ind = row1;


      x = (double**)calloc(row1,sizeof(double*));

      for(i = 0 ; i < row1 ; i++)
      {
       x[i] = (double*)calloc(col1,sizeof(double));
      }

  }


  void Redimension(int row1, int col1)
  {

     int i;
     col_ind = col1;
     row_ind = row1;


      x = (double**)realloc(x,row1*sizeof(double*));
      if ( x == NULL) {cout<<"Memory allocation failed ";exit(0); }

      for(i = 0 ; i < row1 ; i++)
      {
       x[i] = (double*)realloc(x[i],col1*sizeof(double));
       if ( x[i] == NULL) {cout<<"Memory allocation failed ";exit(0); }

      }


  }

  void print()
  {
     int i,j;

   for (i = 0 ; i < row_ind ; i++)
    {
    for(j = 0 ; j < col_ind ; j++)
     {
         printf("%lf \t",x[i][j]);
     }
        printf("\n");
    }

    printf("\n");

  }

 void print_sub(int k)
  {
     int i,j;

   for (i = 0 ; i < k ; i++)
    {
    for(j = 0 ; j < k ; j++)
     {
         printf("%lf \t",x[i][j]);
     }
        printf("\n");
    }

    printf("\n");

  }

  void print_wthindex()
  {
     int i,j;

   for (i = 0 ; i < row_ind ; i++)
    {
    for(j = 0 ; j < col_ind ; j++)
     {
         printf("%lf (%d,%d) \t",x[i][j],i,j);
     }
        printf("\n \n \n");
    }

    printf("\n \n \n \n");

  }

  void print_diagonal()
  {

   int i,j;

   cout<<endl<<" Diagonal elements "<<endl;

    for ( i = 0 ; i < row_ind ; i++)
    {

      cout<<x[i][i]<<endl;

    }

  }

  void print_compressed()
  {

   int i,j;

   for( i = 0 ; i < row_ind ; i++)
    {
     for( j = 0 ; j < col_ind ; j++)
         {

            if(x[i][j] != 0) printf(" %lf (%d,%d) ",x[i][j],i,j);

         }

         cout<<endl<<endl;
    }


  }

  double* getrow(int k)
  {
      int i;
      double *temprow;

        temprow = (double*)calloc(col_ind,sizeof(double));

        for(i = 0 ; i < col_ind ; i++)
        {

         temprow[i] = x[k][i]  ;
        }

 return(temprow);
  }


   double* getcol(int k)
  {
      int i;
      double *temprow;

        temprow = (double*)calloc(row_ind,sizeof(double));

        for(i = 0 ; i < row_ind ; i++)
        {

         temprow[i] = x[i][k]  ;
        }

 return(temprow);
  }


  void column_replace(Vec &A,int k)
  {

    int i;

    for(i = 0 ; i < A.col_ind ; i++)
     x[i][k] = A.x[i];

  }

  void column_replace(double *A,int k)
  {

    int i;

    for(i = 0 ; i < row_ind ; i++)
     x[i][k] = A[i];

  }


  void row_replace(Vec &A,int k)
  {

    int i;

    for(i = 0 ; i < A.col_ind ; i++)
     x[k][i] = A.x[i];


  }

  double **x;




  int row_ind,col_ind ;


};



class Comp_Matrix
{

  public:
  Comp_Matrix()
  {

     x = NULL; ia = NULL; ja = NULL ; my_rank = -1; np = 1; nrow = 0; ncol = 0; num_elem = 0 ;

  }

  ~Comp_Matrix()
  {
     int i;

     if( nrow > 0  )
     {

       free(ia);
       free(ja);
       free(x);


     }
  }


   Comp_Matrix(int row1,int col1, int size, int rank, int p)
  {
      int i;
      ncol = col1;
      nrow = row1;
      num_elem = size;
      my_rank = rank;
      np = p;

      x = (double*)calloc(size,sizeof(double));
      ia = (int*)calloc(nrow + 1,sizeof(double));
      ja = (int*)calloc(size,sizeof(double));

  }



  void Redimension(int row1,int col1, int size, int rank, int p)
  {
      int i;
      ncol = col1;
      nrow = row1;
      num_elem = size;
      my_rank = rank;
      np = p;

      x = (double*)realloc(x,size*sizeof(double));
      ia = (int*)realloc(ia,(nrow + 1)*sizeof(double));
      ja = (int*)realloc(ja,size*sizeof(double));
  }


  int index(int i,int k)
  {

   int j;
   int element_index = -1;

   cout<<ia[0]<<endl;
   cout<<ia[1]<<endl;
   cout<<ia[2]<<endl;

    for( j = ia[i] ; j <  ia[i+1] ; j++)
             {

                  if (ja[j] == k) { element_index = j; break; }

             }


   return (element_index);

  }


   Col* getcol(int k)
  {

      int i,j;
      Col temprow;
      int pos = 0;

        //temprow = (Col*)calloc(1,sizeof(double));


        for(i = 0 ; i < nrow ; i++)
        {

           pos = index(i,k);

              if (pos != -1)
              {

                  temprow.Add(x[pos],i);
                  break;

              }

        }

 return(&temprow);
  }



  void print()
  {


      int i,j;

       for(i = 0 ; i < nrow ; i++)
        {
             for( j = ia[i] ; j <  ia[i+1] ; j++)
             {

              if (index(i,j) != -1)
              {

                 cout<<x[j]<<"  ";

              }

              else cout<<0<<"  ";

             }

            cout<<endl;
        }

  }




void column_replace(Col &A,int k)
  {

    int i;
    int row,pos;

    for(i = 0 ; i < A.col_ind ; i++)
        {

           row = A.row_ptr[i];
           pos = index(row,k);

              if (pos != -1)
              {

                 x[pos] = A.x[i];

              }

        }


  }


  void print_ia_ja()
{

    int i,j;
    FILE *fp;

    if ((fp=fopen("Ia_ja.dat","w")) == 0)
    {
        printf("Unable to open file \n");
    }


    fprintf(fp,"%d \n",nrow);

    for ( j = 0 ; j < nrow ; j++)
    {
        for (i = ia[j] ; i < ia[j+1] ; i++)
        {
            fprintf(fp," %lf--(%d) \t",x[i],ja[i]);
        }
        fprintf(fp,"\n");
    }
    fclose(fp);

}



void scalar_mult(Col &A,double scalar,Col &B)
{

 int i,j,k;
 double temp;

            for(j=0;j< A.col_ind;j++)
            {

               B.x[j] = scalar*A.x[j];
               B.row_ptr[j] = A.row_ptr[j];

            }

}


void scalar_mult(Vec &A,double *prod, double scalar)
{

 int i,j,k;

            for(j=0;j< A.col_ind;j++)
            {

               prod[j] = scalar*A.x[j];

            }

}



void mat_sub(Col &A,Col &B,Col &C)
{


 int i,j;

 for (i = 0 ; i < A.col_ind ; i++)
    C.x[i] = A.x[i] - B.x[i];

}



  ///////  VARIABLES /////

  int nrow,ncol;
  int *ia,*ja;
  double *x;
  int np;
  int my_rank;
  int num_elem;

};


double norm(Col &A)
{

    int i;
    double b = 0.0;


    for (i = 0 ; i < A.col_ind ; i ++)
      b += A.x[i]*A.x[i];

    return (sqrt(b));

}



void mat_copy(Matrix &A, Matrix &B , int size , int flag)
{

    int i,j,k;


 for(i = 0; i < size ; i++)
        {
            for(j = 0; j < size ; j++)
            {

               B.x[i][j] = A.x[i][j];


            }
            //printf("\n");
        }


}




void mat_copy(Matrix &A,Matrix &B)
{

    int i,j,k;


 for(i=0;i< A.row_ind;i++)
        {
            for(j=0;j< A.col_ind;j++)
            {

               B.x[i][j] = A.x[i][j];


            }
            //printf("\n");
        }



}


void mat_copy(Vec &A,Vec &B)
{

    int i,j,k;


            for(j=0;j< A.col_ind;j++)
            {

               B.x[j] = A.x[j];
           }

}


void mat_copy(Matrix &A,Vec &B)
{

    int i,j,k;


            for(j=0;j< A.row_ind;j++)
            {

               B.x[j] = A.x[j][0];
           }

}


void mat_copy(Vec &A,Matrix &B)
{

    int i,j,k;

            for(j=0;j< B.row_ind;j++)
            {

               B.x[j][0] = A.x[j];
           }

}


void mat_copy(double *A,Matrix &B,int size)
{

    int i,j,k;

            for(j=0;j< size;j++)
            {

               B.x[j][0] = A[j];
           }

}


void mat_copy( Matrix &A, double *B, int size )
{

    int i,j,k;


            for(j=0;j< size ;j++)
            {

               B[j] = A.x[j][0];
           }

}


void mat_mul(Matrix &A,Matrix &B,Matrix &C)
{

 int i,j,k;

 int r3,c3;
 r3 = C.row_ind;
 c3 = C.col_ind;

 Matrix tmp(r3,c3);

 mat_copy(C,tmp);

 double temp;

 for(i=0;i<A.row_ind;i++)
        {
            for(j=0;j<B.col_ind;j++)
            {
                tmp.x[i][j]=0.0;
                for(k=0;k<A.col_ind;k++)
                {
                    tmp.x[i][j] += A.x[i][k]*B.x[k][j];

                }
               //printf("%d\t",mult[i][j]);
            }
            //printf("\n");
        }

mat_copy(tmp,C);

}


void mat_mul_e(Matrix &A,Vec &B,Vec &C )
{

 int i,j,k;
 int row1,col1,row2;

 row1 = A.row_ind ;
 col1 = A.col_ind ;
 row2 = B.col_ind ;

 int r3,c3;
 r3 = row2;
 c3 = 1;

 Matrix tmp(r3,c3);

 if ( col1 != row2  ) { cout<<"Dimension mismatch in mat multiplication "<<endl; exit(0);}


 mat_copy(C,tmp);

 double temp;

 for(i=0;i < row1 ;i++)
        {
            for(j=0;  j<1 ;j++)
            {
                tmp.x[i][j]=0.0;
                for(k = 0; k < col1  ; k++)
                {
                    tmp.x[i][j] += A.x[i][k]*B.x[k];

                }
               //printf("%d\t",mult[i][j]);
            }
            //printf("\n");
        }

mat_copy(tmp,C);

}


void  mat_mul( Comp_Matrix &A, double *B, double *C,int vec_size)
{

 int i,j,k;
 int row1,col1,row2;

 double temp, sum;
 int  count;
 count = 0;

 if ( A.nrow > vec_size ) { cout<<"Dimension mismatch im Comp mat_mul \n"; exit(0);}


  for ( i = 0 ; i < A.nrow ; i++)
    {

        sum = 0.0;
        for ( j =  A.ia[i] ; j < A.ia[i + 1] ; j++)
        {

            sum += A.x[count] * B[A.ja[count]] ;
            count++ ;
        }

        C[i] = sum;
    }


}


void mat_mul(Matrix &A,Matrix &B,Matrix &C, int size)
{

 int i,j,k;

 int r3,c3;
 r3 = size;
 c3 = size;

 Matrix tmp(r3,c3);

 mat_copy(C, tmp , size , 1);

 double temp;

 for(i = 0; i < size; i++)
        {
            for( j = 0; j < size ; j++ )
            {
                tmp.x[i][j]=0.0;

                for( k = 0; k < size ; k++)
                {
                    tmp.x[i][j] += A.x[i][k]*B.x[k][j];

                }
               //printf("%d\t",mult[i][j]);
            }
            //printf("\n");
        }

mat_copy(tmp, C , size , 1 );

}



void mat_mul(Matrix &A,Vec &B,Vec &C)
{

 int i,j,k;

 int r3,c3;
 r3 = C.col_ind;
 c3 = 1;

 Matrix tmp(r3,c3);

 mat_copy(C,tmp);

 double temp;

 for(i=0;i<A.row_ind;i++)
        {
            for(j=0;  j<1 ;j++)
            {
                tmp.x[i][j]=0.0;
                for(k=0;k<A.col_ind;k++)
                {
                    tmp.x[i][j] += A.x[i][k]*B.x[k];

                }
               //printf("%d\t",mult[i][j]);
            }
            //printf("\n");
        }

mat_copy(tmp,C);

}


void mat_mul(Vec &A,Vec &B,double &C)
{

 int i,j,k;

 int r3,c3;


 Vec tmp(r3);

 double temp;
 C = 0.0;

            for(j=0;j<B.col_ind;j++)
            {

                    C += A.x[j]*B.x[j];

            }

}




double norm(Vec &A)
{

    int i;
    double b = 0.0;


    for (i = 0 ; i < A.col_ind ; i ++)
      b += A.x[i]*A.x[i];

    return (sqrt(b));

}

void transpose(Matrix &A,Matrix &B)
{

 int i,j,k;


 double temp;
 int r3,c3;
 r3 = B.row_ind;
 c3 = B.col_ind;

 Matrix tmp(r3,c3);
 mat_copy(B,tmp);

 for(i=0;i< A.row_ind;i++)
        {
            for(j=0;j< A.col_ind;j++)
            {

               tmp.x[j][i] = A.x[i][j];

            }
            //printf("\n");
        }

     mat_copy(tmp,B);


}

void inner_product(Vec &A,Vec &B,double &result)
{

 int i,j,k;


 double temp;
 result = 0;

            for(j=0;j< A.col_ind;j++)
            {

              result += B.x[j] * A.x[j];

            }

}

void outer_product(Vec &A,Vec &B,Matrix &result)
{

 int i,j,k;


 double temp;


 for(i=0;i< result.row_ind;i++)
        {
            for(j=0; j< result.col_ind;j++)
            {

              result.x[i][j] = B.x[j] * A.x[i];
              temp = result.x[i][j];

            }
        }



}


void scalar_mult(Matrix &A,Matrix &B,double scalar)
{

 int i,j,k;


 double temp;

 for(i=0;i< A.row_ind;i++)
        {
            for(j=0;j< A.col_ind;j++)
            {

               B.x[i][j] = scalar*A.x[i][j];
               temp =  B.x[i][j] ;

            }
            //printf("\n");
        }




}

void scalar_mult(Vec &A,Vec &B,double scalar)
{

 int i,j,k;


 double temp;


            for(j=0;j< A.col_ind;j++)
            {

               B.x[j] = scalar*A.x[j];

            }
            //printf("\n");





}


void scalar_mult(Vec &A,double *prod, double scalar)
{

 int i,j,k;

            for(j=0;j< A.col_ind;j++)
            {

               prod[j] = scalar*A.x[j];

            }

}



////////////////////////////////////////////////////////////////

int sign(double v)
{
if (v < 0)
return -1.0;
else return 1.0;
}


void print_tofile_xy(double *x,double *y,int nn,char buff[132])
{

    int i;
    FILE *fp;

    if ((fp=fopen(buff,"w")) == 0)
    {
        printf("Unable to open file \n");
    }



    fprintf(fp,"%d \n",nn);


    for (i=0; i < nn; i++)
    {

        fprintf(fp,"%19.10e %19.10e \n",x[i],y[i]);

    }

    fclose(fp);

}

void print_tofile_xy(double *x,int nn,char buff[132])
{

    int i;
    FILE *fp;

    if ((fp=fopen(buff,"w")) == 0)
    {
        printf("Unable to open file \n");
    }



    fprintf(fp,"%d \n",nn);


    for (i=0; i < nn; i++)
    {

        fprintf(fp,"%19.10e \n",x[i]);

    }

    fclose(fp);

}


void print_tofile_xy(int *x,int nn,char buff[132])
{

    int i;
    FILE *fp;

    if ((fp=fopen(buff,"w")) == 0)
    {
        printf("Unable to open file \n");
    }



    fprintf(fp,"%d \n",nn);


    for (i=0; i < nn; i++)
    {

        fprintf(fp,"%d \n",x[i]);

    }

    fclose(fp);

}

void print_file_node(Node *matrix,int nn,char buff[132])
{

    int i,j;
    FILE *fp;

    if ((fp=fopen(buff,"w")) == 0)
    {
        printf("Unable to open file \n");
    }



    fprintf(fp,"%d \n",nn);


    for ( i = 0 ; i < nn ; i++)
    {

        for(j = 0 ; j < ind ; j++)
        {
        fprintf(fp," %lf \t",matrix[i].element[j]);
        }
        fprintf(fp,"\n");
    }
    fclose(fp);

}




Node mat_mul(Block A,Node B)
{

 int i,j,k;
 Node C;

 int r1 = ind;
 int c2 = 1 ;

 double temp;

 for(i=0;i<r1;i++)
        {
            for(j=0;j<c2;j++)
            {
                C.element[i]=0.0;
                for(k=0;k<r1;k++)
                {
                    C.element[i] += A.element[i][k]*B.element[k];
                    temp  = C.element[i];
                    /*mult[0][0]=m1[0][0]*m2[0][0]+m1[0][1]*m2[1][0]+m1[0][2]*m2[2][0];*/
                }
               //printf("%d\t",mult[i][j]);
            }
            //printf("\n");
        }

        return C;


}


Node neg(Node A)
{
    Node C;
    int i ;

    for(i = 0; i < ind ; i++)
    {
    C.element[i] = -1.0*A.element[i];
    }
    return C;
}


Node mat_sub_sq(Node A,Node B)
{

Node C;
 int i,j;

 for (i = 0 ; i < ind ; i++)
   C.element[i] = (A.element[i] - B.element[i])*(A.element[i] - B.element[i]);

 return C;


}


Node mat_add(Node A,Node B)
{

 Node C;
 int i,j;

 for (i = 0 ; i < ind ; i++)
    C.element[i] = A.element[i] + B.element[i];

 return C;

}

/////////////////// Block routines ///////////


Block mat_mul(Block A,Block B)
{

  Block C;
  int i,j,k;
  double sum;

  for (i = 0; i < ind; i++) {
    for (j = 0; j < ind; j++) {
              sum = 0.0;
                  for (k = 0; k < ind; k++) {
                      sum += A.element[i][k] * B.element[k][j];
                   }
              C.element[i][j] = sum;
       }
  }

 return C;
}


Block mat_add(Block A,Block B)
{

 Block C;
 int i,j;

 for (i = 0 ; i < ind ; i++)
   for (j = 0 ; j < ind ; j++)
 C.element[i][j] = A.element[i][j] + B.element[i][j];

 return C;

}


Block mat_sub(Block A,Block B)
{

 Block C;
 int i,j;

 for (i = 0 ; i < ind ; i++)
   for (j = 0 ; j < ind ; j++)
 C.element[i][j] = A.element[i][j] - B.element[i][j];

 return C;

}



////////////////////////////////////////////



void Invert2(double *mat, double *dst)
  {
  double            tmp[12]; /* temp array for pairs                   */
  double            src[16]; /* array of transpose source matrix */
  double            det;             /* determinant                    */
  /* transpose matrix */
  for (int i = 0; i < 4; i++) {
          src[i]                    = mat[i*4];
          src[i + 4]                = mat[i*4 + 1];
          src[i + 8]                = mat[i*4 + 2];
          src[i + 12]               = mat[i*4 + 3];
  }
  /* calculate pairs for first 8 elements (cofactors) */
  tmp[0]         =   src[10]        *   src[15];
  tmp[1]         =   src[11]        *   src[14];
  tmp[2]         =   src[9]         *   src[15];
  tmp[3]         =   src[11]        *   src[13];
  tmp[4]         =   src[9]         *   src[14];
  tmp[5]         =   src[10]        *   src[13];
  tmp[6]         =   src[8]         *   src[15];
  tmp[7]         =   src[11]        *   src[12];
  tmp[8]         =   src[8]         *   src[14];
  tmp[9]         =   src[10]        *   src[12];
  tmp[10]        =   src[8]         *   src[13];
  tmp[11]        =   src[9]         *   src[12];
  /* calculate first 8 elements (cofactors) */
  dst[0]         =   tmp[0]*src[5]            +  tmp[3]*src[6]         + tmp[4]*src[7];
  dst[0]       -=    tmp[1]*src[5]            +  tmp[2]*src[6]         + tmp[5]*src[7];
  dst[1]         =   tmp[1]*src[4]            +  tmp[6]*src[6]         + tmp[9]*src[7];
  dst[1]       -=    tmp[0]*src[4]            +  tmp[7]*src[6]         + tmp[8]*src[7];
  dst[2]         =   tmp[2]*src[4]            +  tmp[7]*src[5]         + tmp[10]*src[7];
  dst[2]       -=    tmp[3]*src[4]            +  tmp[6]*src[5]         + tmp[11]*src[7];
  dst[3]         =   tmp[5]*src[4]            +  tmp[8]*src[5]         + tmp[11]*src[6];
  dst[3]       -=    tmp[4]*src[4]            +  tmp[9]*src[5]         + tmp[10]*src[6];
  dst[4]          =  tmp[1]*src[1]            +  tmp[2]*src[2]         + tmp[5]*src[3];
  dst[4]       -=    tmp[0]*src[1]            +  tmp[3]*src[2]         + tmp[4]*src[3];
  dst[5]          =  tmp[0]*src[0]            +  tmp[7]*src[2]         + tmp[8]*src[3];
  dst[5]       -=    tmp[1]*src[0]            +  tmp[6]*src[2]         + tmp[9]*src[3];
  dst[6]          =  tmp[3]*src[0]            +  tmp[6]*src[1]         + tmp[11]*src[3];
  dst[6]       -=    tmp[2]*src[0]            +  tmp[7]*src[1]         + tmp[10]*src[3];
  dst[7]          =  tmp[4]*src[0]            +  tmp[9]*src[1]         + tmp[10]*src[2];
  dst[7]       -=    tmp[5]*src[0]            +  tmp[8]*src[1]         + tmp[11]*src[2];
  /* calculate pairs for second 8 elements (cofactors) */
  tmp[0]         =   src[2]*src[7];
  tmp[1]         =   src[3]*src[6];
  tmp[2]         =   src[1]*src[7];
  tmp[3]         =   src[3]*src[5];
  tmp[4]         =   src[1]*src[6];

  tmp[5]         =   src[2]*src[5];


  tmp[6]        =   src[0]*src[7];
  tmp[7]        =   src[3]*src[4];
  tmp[8]        =   src[0]*src[6];
  tmp[9]        =   src[2]*src[4];
  tmp[10]       =   src[0]*src[5];
  tmp[11]       =   src[1]*src[4];
  /* calculate second 8 elements (cofactors) */
  dst[8] =          tmp[0]*src[13] + tmp[3]*src[14] + tmp[4]*src[15];
  dst[8] -=         tmp[1]*src[13] + tmp[2]*src[14] + tmp[5]*src[15];
  dst[9] =          tmp[1]*src[12] + tmp[6]*src[14] + tmp[9]*src[15];
  dst[9] -=         tmp[0]*src[12] + tmp[7]*src[14] + tmp[8]*src[15];
  dst[10] =         tmp[2]*src[12] + tmp[7]*src[13] + tmp[10]*src[15];
  dst[10]-=         tmp[3]*src[12] + tmp[6]*src[13] + tmp[11]*src[15];
  dst[11] =         tmp[5]*src[12] + tmp[8]*src[13] + tmp[11]*src[14];
  dst[11]-=         tmp[4]*src[12] + tmp[9]*src[13] + tmp[10]*src[14];
  dst[12] =         tmp[2]*src[10] + tmp[5]*src[11] + tmp[1]*src[9];
  dst[12]-=         tmp[4]*src[11] + tmp[0]*src[9] + tmp[3]*src[10];
  dst[13] =         tmp[8]*src[11] + tmp[0]*src[8] + tmp[7]*src[10];
  dst[13]-=         tmp[6]*src[10] + tmp[9]*src[11] + tmp[1]*src[8];
  dst[14] =         tmp[6]*src[9] + tmp[11]*src[11] + tmp[3]*src[8];
  dst[14]-=         tmp[10]*src[11] + tmp[2]*src[8] + tmp[7]*src[9];
  dst[15] =         tmp[10]*src[10] + tmp[4]*src[8] + tmp[9]*src[9];
  dst[15]-=         tmp[8]*src[9] + tmp[11]*src[10] + tmp[5]*src[8];
  /* calculate determinant */
  det=src[0]*dst[0]+src[1]*dst[1]+src[2]*dst[2]+src[3]*dst[3];
  /* calculate matrix inverse */
  det = 1/det;
  for (int j = 0; j < 16; j++)
         dst[j] *= det;
  }


  void f_print_eig(double *eigen, int k, int i, FILE *fp)
  {

    int j;
    double eig;

    for ( j = 0 ; j < k ; j++)
    {

        fprintf(fp,"%lf \t",eigen[j]);


    }

    fprintf(fp,"\n");


  }
